<?php
/**
 * Created by PhpStorm.
 * User: rjeurissen
 * Date: 01-03-18
 * Time: 13:08
 */

namespace GetNoticed\HtmlWidget\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Html extends Template implements BlockInterface
{
    protected $_template = "widget/content.phtml";
}